import React from "react";

export default class Button extends React.Component {
  render() {
    return (
      <React.Fragment>
        <button className={this.props.class} onClick={this.onclick}>
          {this.props.name}
        </button>
      </React.Fragment>
    );
  }
}
