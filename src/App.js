import React from "react";
import "./App.css";
import Header from "./component/header/header";
import Home from "./component/home/home";
import Start from "./component/start/start";
import API from "./component/api/api";
import Services from "./component/services/services";
import Getstarted from "./component/getstarted/getstarted";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <React.Fragment>
      <Router>
        <Header />
        <React.Fragment>
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/Start" component={Start} />
          <Route exact path="/api" component={API} />
          <Route exact path="/services" component={Services} />
          <Route exact path="/getstarted" component={Getstarted} />
        </React.Fragment>
      </Router>
    </React.Fragment>
  );
}

export default App;
