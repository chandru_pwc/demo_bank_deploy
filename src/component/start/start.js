import React from "react";
import "../start/start.scss";
import Button from "../../common/button";
import Onboard from "../../images/onboard_icon.svg";
import Innovate from "../../images/innovate_icon.svg";
import Publish from "../../images/publish_icon.svg";
export default class Start extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="start-wrap">
          <div className="start-content">
            <div className="Startcontent">
              <div className="startcontent-title">
                <p>
                  BANKING INNOVATION
                  <br />
                  IN ACTION
                </p>
                <div className="startcontent-button">
                  <Button class="button-text" name="Get Started" />
                </div>
              </div>
            </div>
          </div>
          <div className="start-footer">
            <div className="footer-text">
              <div className="panel-txt-wrap">
                <div>
                  <img className="panel-icon" src={Onboard} alt={"Onboard"} />
                </div>
                <div>
                  <span className="panel-title">ONBOARD</span>
                </div>
                <span className="panel-txt">
                  consectetuer adipiscing elit. Aenean commodo ligula eget
                  dolor. Aenean massa cum sociis natoque.
                </span>
              </div>
              <div className="panel-txt-wrap">
                <div>
                  <img className="panel-icon" src={Innovate} alt={"Innovate"} />
                </div>
                <div>
                  <span className="panel-title">INNOVATE</span>
                </div>
                <span className="panel-txt">
                  Etiam rhoncus. Maecenas tempus, tellus eget condimentum
                  rhoncus, sem quam semper libero.
                </span>
              </div>
              <div className="panel-txt-wrap">
                <div>
                  <img className="panel-icon" src={Publish} alt={"Publish"} />
                </div>
                <div>
                  <span className="panel-title">PUBLISH</span>
                </div>
                <span className="panel-txt">
                  neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
                  hendrerit id, lorem.
                </span>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
