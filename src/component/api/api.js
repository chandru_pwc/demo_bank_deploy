import React from "react";
import "../api/api.scss";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import APIDocs from "../APIDocs/apiDocs";

export default class Api extends React.Component {
  state = {
    menuitem: [
      { val: "Overview" },
      { val: "API Docs", active: true, path: "/apiDocs" },
      { val: "Sample" },
      { val: "Tutorial" },
      { val: "Support" }
    ],
    // menuitem: [{ val: 'Overview', path: '/overview', active: true }, { val: 'API Docs', path: '/apiDocs' }, { val: 'Sample', path: '/sample' },
    // { val: 'Tutorial', path: '/tutorial' }, { val: 'Support', path: '/support' }],
    count: 0
  };
  Apirouting = (item, ind) => {
    let { menuitem, count } = this.state;
    count = ind;
    menuitem.forEach(element => {
      if (item.val === element.val) element.active = true;
      else element.active = false;
    });
    this.setState({ menuitem, count });
  };

  render() {
    return (
      <React.Fragment>
        <div className="api-wrap">
          <div className="api-navbar">
            {this.state.menuitem.map((item, i) => (
              <NavLink to={item.path} key={i}>
                <div className="navbar-width">
                  <div
                    className={`api-header ${
                      this.state.count === i ? "active" : ""
                    }`}
                    onClick={$event => this.Apirouting(item, $event, i)}
                  >
                    {item.val}
                  </div>
                </div>
              </NavLink>
            ))}
          </div>
          <APIDocs />
        </div>
      </React.Fragment>
    );
  }
}
