import React from "react";
import "../home/home.scss";
import Button from "../../common/button";
export default class Home extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="home-wrap">
          <div className="content-wrap">
            <div className="Homecontent">
              <div className="content-text">
                <p>
                  Welcome to the <span className="name-strong">DEMO BANK</span>
                  <br />
                  Developer Portal
                </p>
                <div className="content-subtitle">
                  <p>Create new banking experiences with our APIs</p>
                </div>
                <div className="content-button">
                  <Button class="button-text" name="Get Started" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
