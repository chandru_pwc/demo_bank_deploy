import React from "react";
import "../header/header.scss";
import Logo from "../../images/logo.png";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, NavLink } from "react-router-dom";
import Button from "../../common/button";
export default class Header extends React.Component {
  state = {
    tabitem: ["Home", "About", "Services", "Contact"],
    navItems: [
      { val: "Home", path: "/home", active: true },
      { val: "About", path: "/Start" },
      { val: "APIs", path: "/api" },
      { val: "Services", path: "/services" },
      { val: "Get Started", path: "/getstarted" }
    ],
    navlink: [],
    count: 0
  };
  routing = (item, ind) => {
    let { navItems, count } = this.state;
    count = ind;
    navItems.forEach(element => {
      if (item.val === element.val) element.active = true;
      else element.active = false;
    });
    this.setState({ navItems, count });

    if (item.val === "About") this.setState({ aboutActive: true });
    if (item.val !== "About") this.setState({ aboutActive: false });
  };
  render() {
    return (
      <React.Fragment>
        <div className="header-wrap">
          <div className="header-logo">
            <img src={Logo} alt="image" className="logo" />
          </div>
          <div
            className={`navbar-wrap ${
              this.state.aboutActive ? "Active-header" : ""
            }`}
          >
            <div className="header-navbar">
              {this.state.navItems.map((item, i) => (
                <NavLink to={item.path} key={i}>
                  <div
                    className={`navbar ${
                      this.state.count === i ? "active" : ""
                    } ${this.state.aboutActive ? "header" : ""}`}
                    onClick={$event => this.routing(item, $event, i)}
                  >
                    {item.val}
                  </div>
                </NavLink>
              ))}
            </div>
          </div>
          <div className="user-wrap">
            <div className="register">
              <Button
                class={`register-button ${
                  this.state.aboutActive ? "about-button" : ""
                }`}
                name="Register"
              />
            </div>
            <div className="login">
              <Button class="login-button" name="Login" />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
