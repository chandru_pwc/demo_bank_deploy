import React from "react";
import "../APIDocs/apiDocs.scss";
import SwaggerUI from "swagger-ui";
import "../../../node_modules/swagger-ui/dist/swagger-ui.css";

export default class Apidocs extends React.Component {
  state = {
    count: 0,
    listItems: [
      { val: "Accounts" },
      { val: "Deposits" },
      { val: "Loan" },
      { val: "Insurance" },
      { val: "Payments" },
      { val: "Investments" },
      { val: "Cards" },
      { val: "Forex" }
    ],
    definitionLink: "./swagger.json"
  };
  sidenavRouting = (item, e) => {
    let { listItems, count } = this.state;
    count = e;
    listItems.forEach(element => {
      if (item.val === element.val) element.activelist = true;
      else element.activelist = false;
    });
    this.setState({ listItems, count });
  };
  componentDidMount() {
    SwaggerUI({
      domNode: document.getElementById("api-data"),
      url: this.state.definitionLink
    });
    setTimeout(() => {
      document.querySelector(".information-container").style.display = "none";
      document.querySelector(".scheme-container").style.display = "none";
    }, 1000);
  }

  render() {
    return (
      <React.Fragment>
        <div className="api-content">
          <div className="sidenav">
            <div className="api-sidenav">
              {this.state.listItems.map((item, i) => (
                <div
                  className={`listitems ${
                    this.state.count === i ? "activelist" : ""
                  }`}
                  onClick={$event => this.sidenavRouting(item, $event, i)}
                >
                  <p>{item.val}</p>
                </div>
              ))}
            </div>
          </div>
          <div className="App">
            <div id="api-data" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
